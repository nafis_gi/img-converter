import { Command } from 'commander';
import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';

import {
  INPUT_DICRETORY_PATH,
  OUTPUT_DICRETORY_PATH,
  IMG_EXTENSIONS,
} from './constants';

const program = new Command();

program
  .command('convert')
  .option(
    '-i, --input-directory <path>',
    'input folder path',
    INPUT_DICRETORY_PATH
  )
  .option(
    '-o, --output-directory <path>',
    'output folder path',
    OUTPUT_DICRETORY_PATH
  )
  .action(({ inputDirectory, outputDirectory }) => {
    if (!fs.existsSync(outputDirectory)) {
      fs.mkdirSync(outputDirectory);
    }

    fs.readdir(inputDirectory, (err, files) => {
      if (err) throw err;

      const images = files.filter(file => {
        const extname = path.extname(file);
        return IMG_EXTENSIONS.includes(extname);
      });

      images.forEach(image => {
        sharp(`${inputDirectory}/${image}`)
          .webp()
          .toFile(
            `${outputDirectory}/${path.parse(image).name}.webp`,
            (err, info) => {
              if (err) throw err;
              console.log(`${image} converted to ${info.format}`);
            }
          );
      });
    });
  });

program.parse();
